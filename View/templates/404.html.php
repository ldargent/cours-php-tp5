<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>PHP TP4 - MVC POO</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/public/css/bootstrap.min.css">
    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>
    <link rel="stylesheet" href="/public/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/public/css/main.css">

    <script src="/public/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Home</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" role="form" action="/user/login/" method="post">
                <div class="form-group">
                    <input name="login" type="text" placeholder="Login" class="form-control">
                </div>
                <div class="form-group">
                    <input name="password" type="password" placeholder="Password" class="form-control">
                </div>
                <button type="submit" class="btn btn-success">Sign in</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Php TP 4!</h1>
        <p>A simple oriented object MVC</p>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <?php if (isset($breadcrumb) && is_array($breadcrumb)) : ?>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Library</a></li>
                    <li class="active">Data</li>
                </ol>
            <?php endif ?>

            <!-- BEGIN : 404.html.php -->
            <img class="img-responsive" src="/public/var/404.jpg" alt="">
            <!-- END : 404.html.php -->

        </div>
    </div>

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>TP Blog - IUT Montpellier</p>
            </div>
        </div>
    </footer>
</div> <!-- /container -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/public/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="/public/js/vendor/bootstrap.min.js"></script>

<script src="/public/js/main.js"></script>
</body>
</html>