<?php

namespace View;

use View\Exceptions\ViewException;

require_once './View/Exceptions/ViewException.php';

/**
 * Class View
 */
class View
{
    const TEMPLATE_FOLDER = "View/templates/";
    const TEMPLATE_EXTENSION = ".html.php";

    /** @var string */
    private $view;

    /** @var array */
    private $params;

    /**
     * View constructor.
     * 
     * @param $view
     * @param array $params
     */
    public function __construct($view, array $params)
    {
        $this->setView($view);
        $this->setParams($params);
    }

    /**
     * @throws ViewException
     */
    public function render()
    {
        // display page content
        echo $this->renderTemplate();
    }

    /**
     * Get content from template file
     *
     * @return string
     * @throws ViewException
     */
    public function renderTemplate()
    {
        // generate template variables
        extract($this->params);

        // start stream
        ob_start();

        $templateFile = self::TEMPLATE_FOLDER . $this->view . self::TEMPLATE_EXTENSION;

        // check if template exists
        if (!file_exists($templateFile)) {
            throw ViewException::templateNotFound($this->view);
        }

        // call template
        require_once $templateFile;

        // return stream
        return ob_get_clean();
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param string $view
     */
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }
}