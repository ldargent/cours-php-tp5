<?php

namespace Framework;

use Controller\Controller;
use Controller\ErrorController;
use Controller\Exceptions\ControllerException;

require_once './Controller/Exceptions/ControllerException.php';
require_once './Controller/ErrorController.php';
require_once './Controller/NotFoundController.php';
require_once './Controller/HomeController.php';
require_once './Controller/ArticleController.php';

/**
 * Class Router
 * @package Framework
 */
class Router
{
    const DEFAULT_CONTROLLER_ACTION = 'view';
    const DEFAULT_CONTROLLER_ID = 'home';
    const NOT_FOUND_CONTROLLER_ID = 'notFound';

    /** @var string */
    private $controller;

    /** @var string */
    private $action;

    /**
     * Router constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param $page
     */
    private function setController($page)
    {
        $this->controller = 'Controller\\' . ucfirst($page) . 'Controller';
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    private function setAction($action)
    {
        $this->action = $action . "Action";
    }

    /**
     * @param Request $request
     */
    public function routeRequest(Request $request)
    {
        // get query parameter for routing
        $page = $request->getQueryParameter('page') ?? self::DEFAULT_CONTROLLER_ID;
        $action = $request->getQueryParameter('action') ?? self::DEFAULT_CONTROLLER_ACTION;

        // set controller name and action
        $this->setController($page);
        $this->setAction($action);

        try {
            // check Controller existence
            if (!class_exists($this->getController())) {
                // set not found controller
                $this->setController(self::NOT_FOUND_CONTROLLER_ID);
                $this->setAction(self::DEFAULT_CONTROLLER_ACTION);
            }

            // check Controller action existence
            $class = new \ReflectionClass($this->getController());
            if (!$class->hasMethod($this->getAction())) {
                throw ControllerException::actionNotFound($this->getController(), $this->getAction());
            }

            // dynamic controller instance
            /** @var Controller $controller */
            $controllerName = $this->getController();
            $controller = new $controllerName($request);

            // run controller's action
            $controller->{$this->getAction()}();

        } catch (\Exception $ex) {
            // run error Controller when an exception has been catch
            $errorController = new ErrorController($request);
            $errorController->setErrorMessage($ex->getMessage());
            $errorController->viewAction();
        }
    }
}