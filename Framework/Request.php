<?php

namespace Framework;

/**
 * Class Request
 * @package Framework
 */
class Request
{
    /** @var array */
    private $query;
    /** @var array */
    private $cookies;
    /** @var array */
    private $session;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->setQuery(array_merge($_GET, $_POST));
        $this->setCookies($_COOKIE);
    }

    /**
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param array $query
     */
    private function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return array
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * @param array $cookies
     */
    private function setCookies($cookies)
    {
        $this->cookies = $cookies;
    }

    /**
     * @return array
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param array $session
     */
    private function setSession($session)
    {
        $this->session = $session;
    }

    /**
     * @param $parameter
     * @return mixed|null
     */
    public function getQueryParameter($parameter)
    {
        // Check if parameter exist into query
        if (isset($this->query[$parameter]) && !empty($this->query[$parameter])) {

            return $this->query[$parameter];
        }

        return null;
    }
}