<?php

namespace Framework;

abstract class Configuration
{
    const blogIniPath = './config/blog.ini';

    /** @var array */
    private static $parameters;

    /**
     * Load parameters from ini
     *
     * @return array
     */
    private static function getParameters()
    {
        // load parameters from ini file
        if (is_null(self::$parameters)) {
            self::$parameters = parse_ini_file(self::blogIniPath, true);
        }

        return self::$parameters;
    }

    /**
     * @param $section
     * @param $parameter
     * @return mixed
     * @throws \Exception
     */
    public static function getParameter($section, $parameter)
    {
        $parameters = self::getParameters();

        if (!isset($parameters[$section][$parameter])) {
            throw new \Exception("Parameter '$parameter' in section '$section' not found.");
        }

        return $parameters[$section][$parameter];
    }
}