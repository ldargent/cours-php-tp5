<?php

use Framework\Request;
use Framework\Router;

require_once './Framework/Request.php';
require_once './Framework/Router.php';

$router = new Router();
$router->routeRequest(new Request());
