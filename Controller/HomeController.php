<?php

namespace Controller;

use Model\Manager\ArticleManager;

require_once './Controller/Controller.php';
require_once './Model/Manager/ArticleManager.php';

class HomeController extends Controller
{
    /**
     * Homepage View action
     */
    public function viewAction()
    {
        $params = array('articles' => ArticleManager::getAll());
        
        $this->render('home', $params);
    }

    /**
     * Custom action example
     */
    public function sendAction()
    {
        $this->redirect('/?page=home');
    }
}