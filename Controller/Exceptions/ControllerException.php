<?php

namespace Controller\Exceptions;

use Controller\Controller;

require_once './Controller/Controller.php';

/**
 * Class ControllerNotFoundException
 * @package Exceptions
 */
class ControllerException extends \Exception
{
    public static function folderNotFound($folder)
    {
        return new self(
            sprintf("Controller folder '%s' doesn't exist.", $folder)
        );
    }

    /**
     * @param $controller
     * @return ControllerException
     */
    public static function notFound($controller)
    {
        return new self(
            sprintf("Class Controller '%s' doesn't exist.", $controller)
        );
    }

    /**
     * @param $controller
     * @param $action
     * @return ControllerException
     */
    public static function actionNotFound($controller, $action)
    {
        return new self(
            sprintf("Action '%s' into controller '%s' doesn't exist.", $action, $controller)
        );
    }
}