<?php

namespace Controller;

require_once './Controller/Controller.php';

class NotFoundController extends Controller
{
    public function viewAction()
    {
        // change http status code response to '404 not found'
        http_response_code(404);

        $this->render('404');
    }
}