<?php

namespace Controller;

require_once './Controller/Controller.php';

class ErrorController extends Controller
{
    private $errorMessage;

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    public function viewAction()
    {
        // change http status code response to '500 internal server error'
        http_response_code(500);

        $params = array('message' => $this->getErrorMessage());

        $this->render('error', $params);
    }
}