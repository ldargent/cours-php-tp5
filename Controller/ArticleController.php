<?php

namespace Controller;

use Model\Manager\ArticleManager;

require_once './Controller/Controller.php';
require_once './Model/Manager/ArticleManager.php';

class ArticleController extends Controller
{
    /**
     * Article View action
     *
     * @throws \Exception
     */
    public function viewAction()
    {
        if (is_null($this->getRequest()->getQueryParameter('id'))) {
            throw new \Exception('Article id is missing');
        }

        $articleId = (int) $this->getRequest()->getQueryParameter('id');

        $params = array('article' => ArticleManager::getById($articleId));

        $this->render('article', $params);
    }
}