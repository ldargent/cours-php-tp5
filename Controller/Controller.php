<?php

namespace Controller;

use Framework\Request;
use View\View;

require_once './View/View.php';

/**
 * Class Controller
 */
abstract class Controller
{
    /** @var Request */
    private $request;

    /**
     * @var View
     */
    private $view;

    /**
     * Controller constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->setRequest($request);
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    private function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return View
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param $viewName
     * @param array $params
     */
    public function setView($viewName, array $params)
    {
        $this->view = new View($viewName, $params);
    }

    /**
     * @return mixed
     */
    abstract public function viewAction();

    /**
     * Render full content (layout include)
     *
     * @param $viewName
     * @param array $params
     */
    public function render($viewName, $params = array())
    {
        // set controller view
        $this->setView($viewName, $params);

        // render view
        $this->getView()->render();
    }

    /**
     * @param $url
     */
    public function redirect($url)
    {
        header('Location: ' . $url);
    }
}